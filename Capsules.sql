-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Anamakine: localhost
-- Üretim Zamanı: 28 Eki 2021, 09:13:41
-- Sunucu sürümü: 10.4.18-MariaDB
-- PHP Sürümü: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `SpaceX`
--

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `Capsules`
--

CREATE TABLE `Capsules` (
  `capsule_serial` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `capsule_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_launch` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_launch_unix` int(11) DEFAULT NULL,
  `missions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`missions`)),
  `landings` int(11) DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reuse_count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Tablo döküm verisi `Capsules`
--

INSERT INTO `Capsules` (`capsule_serial`, `capsule_id`, `status`, `original_launch`, `original_launch_unix`, `missions`, `landings`, `type`, `details`, `reuse_count`, `created_at`, `updated_at`) VALUES
('C101', 'dragon1', 'retired', '2010-12-08T15:43:00.000Z', 1291822980, '[{\"name\":\"COTS 1\",\"flight\":7}]', 1, 'Dragon 1.0', 'Reentered after three weeks in orbit', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C102', 'dragon1', 'retired', '2012-05-22T07:44:00.000Z', 1335944640, '[{\"name\":\"COTS 2\",\"flight\":8}]', 1, 'Dragon 1.0', 'First Dragon spacecraft', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C103', 'dragon1', 'unknown', '2012-10-08T00:35:00.000Z', 1349656500, '[{\"name\":\"CRS-1\",\"flight\":9}]', 1, 'Dragon 1.0', 'First of twenty missions flown under the CRS1 contract', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C104', 'dragon1', 'unknown', '2013-03-01T19:10:00.000Z', 1362165000, '[{\"name\":\"CRS-2\",\"flight\":10}]', 1, 'Dragon 1.0', NULL, 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C105', 'dragon1', 'unknown', '2014-04-18T19:25:00.000Z', 1397849100, '[{\"name\":\"CRS-3\",\"flight\":14}]', 1, 'Dragon 1.1', 'First Dragon v1.1 capsule', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C106', 'dragon1', 'active', '2014-09-21T05:52:00.000Z', 1411278720, '[{\"name\":\"CRS-4\",\"flight\":18},{\"name\":\"CRS-11\",\"flight\":41},{\"name\":\"CRS-19\",\"flight\":85}]', 3, 'Dragon 1.1', 'First Dragon capsule to be reused', 2, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C107', 'dragon1', 'unknown', '2015-01-10T09:47:00.000Z', 1420883220, '[{\"name\":\"CRS-5\",\"flight\":19}]', 1, 'Dragon 1.1', NULL, 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C108', 'dragon1', 'active', '2015-04-14T20:10:00.000Z', 1429042200, '[{\"name\":\"CRS-6\",\"flight\":22},{\"name\":\"CRS-13\",\"flight\":51},{\"name\":\"CRS-18\",\"flight\":82}]', 3, 'Dragon 1.1', 'Second Dragon capsule to be reused', 2, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C109', 'dragon1', 'destroyed', '2015-06-28T14:21:00.000Z', 1435501260, '[{\"name\":\"CRS-7\",\"flight\":24}]', 0, 'Dragon 1.1', 'Destroyed on impact after F9 launch failure', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C110', 'dragon1', 'active', '2016-04-08T20:43:00.000Z', 1460148180, '[{\"name\":\"CRS-8\",\"flight\":28},{\"name\":\"CRS-14\",\"flight\":59}]', 2, 'Dragon 1.1', NULL, 1, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C111', 'dragon1', 'active', '2016-07-18T04:45:00.000Z', 1468817100, '[{\"name\":\"CRS-9\",\"flight\":32},{\"name\":\"CRS-15\",\"flight\":64}]', 2, 'Dragon 1.1', NULL, 1, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C112', 'dragon1', 'active', '2017-02-19T14:39:00.000Z', 1487515140, '[{\"name\":\"CRS-10\",\"flight\":36},{\"name\":\"CRS-16\",\"flight\":72},{\"name\":\"CRS-20\",\"flight\":91}]', 3, 'Dragon 1.1', NULL, 2, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C113', 'dragon1', 'active', '2017-08-14T16:31:00.000Z', 1502728260, '[{\"name\":\"CRS-12\",\"flight\":45},{\"name\":\"CRS-17\",\"flight\":78}]', 2, 'Dragon 1.1', 'The last newly manufactured Dragon 1', 1, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C201', 'dragon2', 'active', '2019-03-02T07:45:00.000Z', 1551512700, '[{\"name\":\"CCtCap Demo Mission 1\",\"flight\":76}]', 1, 'Dragon 2.0', 'Destroyed in a test anomaly at LZ-1 on April 20, 2019', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C202', 'dragon2', 'active', NULL, NULL, '[]', 0, 'Dragon 2.0', 'Capsule used to qualify Dragon 2\'s environmental control and life support systems.', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C203', 'dragon2', 'active', NULL, NULL, '[]', 0, 'Dragon 2.0', 'Rumored to be used for Inflight Abort Test after DM-1', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C204', 'dragon2', 'active', NULL, NULL, '[]', 0, 'Dragon 2.0', 'Currently in construction for use in DM-2', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C205', 'dragon2', 'active', '2020-01-19T14:00:00.000Z', 1579442400, '[{\"name\":\"Crew Dragon In Flight Abort Test\",\"flight\":88}]', 1, 'Dragon 2.0', 'In construction for use in first mission in contract under the CCtCap contract', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49'),
('C206', 'dragon2', 'active', '2020-05-30T19:22:00.000Z', 1590866520, '[{\"name\":\"CCtCap Demo Mission 2\",\"flight\":94}]', 1, 'Dragon 2.0', 'In Hawthorne, CA as of October 11, 2019. Seen in background during Musk/Bridenstine press conference.', 0, '2021-10-28 06:08:49', '2021-10-28 06:08:49');

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `Capsules`
--
ALTER TABLE `Capsules`
  ADD PRIMARY KEY (`capsule_serial`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
