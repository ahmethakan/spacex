<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EndpointTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_all()
    {
        $response = $this->get('/api/capsules');

        $response->assertStatus(200);
    }

    public function test_status()
    {
        $response = $this->get('/api/capsules?status=active');

        $response->assertStatus(200);
    }

    public function test_detail()
    {
        $response = $this->get('/api/capsules/C101');

        $response->assertStatus(200);
    }

}
