<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Capsules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Capsules', function (Blueprint $table) {
            $table->string('capsule_serial')->primary();
            $table->string('capsule_id')->nullable()->default(NULL);
            $table->string('status')->nullable()->default(NULL);
            $table->string('original_launch')->nullable()->default(NULL);
            $table->integer('original_launch_unix')->nullable()->default(NULL);
            $table->json('missions')->nullable()->default(NULL);
            $table->integer('landings')->nullable()->default(NULL);
            $table->string('type')->nullable()->default(NULL);
            $table->string('details')->nullable()->default(NULL);
            $table->integer('reuse_count')->nullable()->default(NULL);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Capsules');
    }
}
