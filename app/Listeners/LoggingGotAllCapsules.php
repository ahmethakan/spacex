<?php

namespace App\Listeners;

use App\Events\GotAllCapsules;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LoggingGotAllCapsules
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GotAllCapsules  $event
     * @return void
     */
    public function handle(GotAllCapsules $event)
    {
        Log::info($event->response);

        Log::info("Sync is complete.");
    }
}
