<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Capsule extends Model
{
    use HasFactory;

    protected $table = 'Capsules';

    public $timestamps = false;

    protected $casts = [
        'missions' => 'array'
    ];
}
