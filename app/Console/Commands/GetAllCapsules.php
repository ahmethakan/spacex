<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Event;

use App\Models\Capsule;

use App\Events\GotAllCapsules;


class GetAllCapsules extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:getallcapsules';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('https://api.spacexdata.com/v3/capsules')->json();

        if (is_array($response) || is_object($response))
        {
            Capsule::truncate();    // Delete all records

            foreach ($response as $capsule)
            {
                $record = new Capsule;

                $record->capsule_serial = $capsule['capsule_serial'];
                $record->capsule_id = $capsule['capsule_id'];
                $record->status = $capsule['status'];
                $record->original_launch = $capsule['original_launch'];
                $record->original_launch_unix = $capsule['original_launch_unix'];
                $record->missions = $capsule['missions'];
                $record->landings = $capsule['landings'];
                $record->type = $capsule['type'];
                $record->details = $capsule['details'];
                $record->reuse_count = $capsule['reuse_count'];

                $record->save();
            }

            // Called event
            event(new GotAllCapsules($response));

            $this->info('Sync is complete.');
        }
    }
}
