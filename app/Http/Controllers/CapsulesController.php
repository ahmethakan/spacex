<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Capsule;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="SpaceX API Documentation",
 *      description="SpaceX capsules",
 *      @OA\Contact(
 *          email="ahmethakan@pm.me"
 *      ),
 *      @OA\License(
 *          name="Apache 2.0",
 *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 * )
 *
 */
class CapsulesController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/capsules",
     *      operationId="getAllCapsules",
     *      tags={"Capsules"},
     *      summary="Get list of capsules",
     *      description="Returns list of capsules",
     *      @OA\Parameter(
     *          name="status",
     *          description="Capsule status",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Records not found."
     *      ),
     *  )
     */
    public function index(Request $request)
    {
        if($request->has('status'))
        {
            $capsules = Capsule::where('status', $request->input('status'))->get();

            if( !empty($capsules[0]) )
            {
                return response()->json($capsules, 201);
            }

            return response()->json('Records not found.', 404);
        }

        $capsules = Capsule::all();

        if( !empty($capsules[0]) )
        {
            return response()->json($capsules, 201);
        }

        return response()->json('Records not found.', 404);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * @OA\Get(
     *      path="/api/capsules/{capsule_serial}",
     *      operationId="getCapsule",
     *      tags={"Capsule"},
     *      summary="Get info of capsule",
     *      description="Returns the capsule",
     *      @OA\Parameter(
     *          name="capsule_serial",
     *          description="Capsule serial",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=201,
     *          description="OK",
     *          @OA\MediaType(
     *              mediaType="application/json",
     *          )
     *      ),
     *      @OA\Response(
     *          response="404",
     *          description="Records not found."
     *      ),
     *  )
     */
    public function show($id)
    {
        $capsule = Capsule::where('capsule_serial', $id)->first();

        if( !empty($capsule) )
        {
            return response()->json($capsule, 201);
        }

        return response()->json('Records not found.', 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
